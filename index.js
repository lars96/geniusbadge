//Changing the refreshrate requires restart. Everything else is updated every timer tick.
var refresh = require('sdk/simple-prefs').prefs['refreshrate'];
var token = "";
var addMsg = false;
var addNews = false;
var addGroups = false;

var { ToggleButton } = require("sdk/ui/button/toggle");
var tabs = require("sdk/tabs");
var {Cc, Ci} = require("chrome");
var promptSvc = Cc["@mozilla.org/embedcomp/prompt-service;1"].
                getService(Ci.nsIPromptService);

var CountAll = 0;

var button = ToggleButton({
    id: "geniusbadgebutton1",
    label: "GeniusBadge",
    onChange: changed,
    icon: {
      "16" : "./genius-16.png",
      "32" : "./genius-32.png",
      "48" : "./genius-48.png"
    },
    badge: "-",
    badgeColor: "black"
  });

function changed(state) {
//Because button.checked is not accessible, neither directly, nor by 'state' argument, disabling and re-enabling is necessary.
  button.state('window', null);
  button.state("window", { disabled: true  });
  button.state("window", { disabled: false });

  if (button.badge == "!") {
    promptSvc.alert(null, "GeniusBadge", "The given authentication token is invalid!");
  }
  else {
    if (button.badge == "X") {
       promptSvc.alert(null, "GeniusBadge", "No authentication token set! You will be brought to the autorization page on Genius if you click right after you click Ok.");
       tabs.open("https://api.genius.com/oauth/authorize?response_type=token&scope=me&client_id=2AD-Zy8tvgXddig2U-qnj04tGMBdnyx9J3Rd5RSXfiXt_9TCmvyK1J8Esx_4S4xk&redirect_uri=https://larsbutnotleast.xyz/genius/auth/&state=1");
    }
    else {
      if (button.badge != "-") {
        tabs.open("http://genius.com");
      }
    }
  }
}

var { setInterval } = require("sdk/timers");
setInterval(function() {
     getSettings();
     checkToken();
}, refresh)


//Retrieve the current settings. This function is called everytime the timer ticks => values can be changed without restart.
function getSettings(){
  token = require('sdk/simple-prefs').prefs['token'];
  addMsg = require('sdk/simple-prefs').prefs['include_msg'];
  addNews = require('sdk/simple-prefs').prefs['include_newsfeed'];
  addGroups = require('sdk/simple-prefs').prefs['include_groups'];
}

//The request will only happen if the given token is set (!= ""), no matter whether it's valid. How should the add-on know? ;)
function checkToken(){
  if (token == "") {
    button.badge = "X";
  }
  else {
    getGeniusData();
  }
}

//This will get the actual data by calling the Genius API. Main activity will be counted every time. Depending on the settings this value will be increased with the value of other fields (message count...)
function getGeniusData(){
  var Request = require("sdk/request").Request;
  var req = Request({
    url: "http://api.genius.com/account",
    headers : {
          "User-Agent": "CompuServe Classic/1.22",
          "Accept": "application/json",
          "Authorization": "Bearer " + token
    },
    onComplete: function (response) {
      switch (response.status) {
          //Token is valid; Notifications will be displayed on the badge
          case 200:
              countNotifications(response);
              break;
          //Token is invalid
          case 401:
              button.badge = "!";
              break;
          //Either there is no internet connection or another HTTP status code occured that has no influence in the script
          case 0:
          default:
              button.badge = "-";
              break;
      }
    }
  }).get();
}

//Will first "initialize" the CountAll var with the current general notification and possibly add other ones to it. The value is displayed on the badge
function countNotifications(response) {
  CountAll = response.json["response"]["user"]["unread_main_activity_inbox_count"];
  if (addMsg)    { CountAll += response.json["response"]["user"]["unread_messages_count"]; }
  if (addNews)   { CountAll += response.json["response"]["user"]["unread_newsfeed_inbox_count"]; }
  if (addGroups) { CountAll += response.json["response"]["user"]["unread_groups_inbox_count"]; }
  button.badge = CountAll;
}

getSettings();
checkToken();
